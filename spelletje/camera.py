# vim:set ts=2 sw=2 sts=2 et:


class Camera(object):
  def __init__(self, engine):
    self.engine = engine
    self.ox = self.oy = 0
    self.maxx = self.maxy = None
    self.minx = self.miny = None
    self.limits = False

  def origin(self):
    return self.ox, self.oy

  def setLimits(self, minx, miny, maxx, maxy):
    self.minx, self.miny = minx, miny
    self.maxx, self.maxy = maxx, maxy
    self.limits = True

  def setLimitsFromMap(self, m):
    mw, mh = m.pixelSize()
    rw, rh = self.engine.render.w, self.engine.render.h
    self.setLimits(0,0,mw-rw,mh-rh)

  def setOrigin(self, x, y):
    if self.limits:
      if x < self.minx: x = self.minx
      if y < self.miny: y = self.miny
      if x > self.maxx: x = self.maxx
      if y > self.maxy: y = self.maxy
    self.ox = x
    self.oy = y

  def centreOn(self, x, y):
    self.setOrigin(x-self.engine.render.w/2,
                   y-self.engine.render.h/2)
