# vim:set ts=2 sw=2 sts=2 et:

from pyglet.window import key
from collections import defaultdict


class Input(object):
  def __init__(self, engine):
    self.engine = engine
    self.mouse = (0,0)
    self.keys = defaultdict(bool)
    self.buttons = defaultdict(bool)
  
  def key_press(self, sym, mods):
    self.keys[sym] = True

  def key_release(self, sym, mods):
    self.keys[sym] = False

  def mouse_motion(self, x, y, dx, dy):
    self.mouse = (x, y)

  def mouse_press(self, x, y, button, mods):
    self.mouse = (x, y)
    self.buttons[button] = True

  def mouse_release(self, x, y, button, mods):
    self.mouse = (x, y)
    self.buttons[button] = False

  def mouse_drag(self, x, y, dx, dy, buttons, mods):
    self.mouse = (x, y)

  def mouse_global(self):
    x, y = self.mouse
    ox, oy = self.engine.camera.origin()
    return x+ox, y+oy
