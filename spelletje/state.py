# vim:set ts=2 sw=2 sts=2 et:


class State(object):
  def __init__(self, engine):
    self.engine = engine

  def update(self, dt):
    pass
  
  def draw(self):
    pass

  def postDraw(self):
    pass

  def enterState(self, args):
    pass

  def leaveState(self):
    pass
  
  def key_press(self, sym, mods):
    pass

  def key_release(self, sym, mods):
    pass
  
  def mouse_motion(self, x, y, dx, dy):
    pass

  def mouse_press(self, x, y, button, mods):
    pass

  def mouse_release(self, x, y, button, mods):
    pass

  def mouse_drag(self, x, y, dx, dy, buttons, mods):
    pass

  def mouse_scroll(self, x, y, sx, sy):
    pass
