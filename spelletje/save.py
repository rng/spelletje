# vim:set ts=2 sw=2 sts=2 et:

import pickle
import os

class Saver(object):
  def __init__(self):
    self.props = {}
    self.fn = "game.save"
    if os.path.exists(self.fn):
      self.props = pickle.load(open(self.fn))

  def get(self, name, default=None):
    return self.props.get(name, default)

  def put(self, name, value):
    self.props[name] = value
    pass

  def write(self):
    pickle.dump(self.props, open(self.fn, "wb"))
