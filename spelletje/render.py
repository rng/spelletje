# vim:set ts=2 sw=2 sts=2 et:

import pyglet
import pyglet.gl as gl
from collections import defaultdict
from .sprite import SpriteSheet
from .font import VariableFont, Font
import ctypes
import math

from .framebuffer import Framebuffer, Texture


class Layer(object):
  def __init__(self):
    self.reset()

  def reset(self):
    self.pos = []
    self.tex = []
    self.col = []
    self.linepos = []
    self.linecol = []
    self.nline = 0
    self.n = 0
  
  def texquad(self, x0,y0,x1,y1,x2,y2,x3,y3, tx0=0,ty0=0,tx1=0,ty1=0, col=None):
    col = col if col else (1,1,1,1)
    self.pos.extend([x0,y0,x1,y1,x2,y2,x3,y3])
    self.tex.extend([tx0,ty0,tx1,ty0,tx1,ty1,tx0,ty1])
    self.col.extend(col*4)
    self.n += 1

  def line(self, x0,y0,x1,y1, col=None):
    col = col if col else (1,1,1,1)
    self.linepos.extend((x0,y0,x1,y1))
    self.linecol.extend(col*2)
    self.nline += 1

  def draw(self):
    gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
    gl.glEnableClientState(gl.GL_TEXTURE_COORD_ARRAY)
    gl.glEnableClientState(gl.GL_COLOR_ARRAY)
    # draw quads
    parr = (ctypes.c_float * len(self.pos))(*self.pos)
    varr = (ctypes.c_float * len(self.tex))(*self.tex)
    carr = (ctypes.c_float * len(self.col))(*self.col)
    gl.glVertexPointer(2, gl.GL_FLOAT, 0, parr)
    gl.glTexCoordPointer(2, gl.GL_FLOAT, 0, varr)
    gl.glColorPointer(4, gl.GL_FLOAT, 0, carr)
    gl.glDrawArrays(gl.GL_QUADS, 0, self.n*4)
    
    parr = (ctypes.c_float * len(self.linepos))(*self.linepos)
    carr = (ctypes.c_float * len(self.linecol))(*self.linecol)
    gl.glVertexPointer(2, gl.GL_FLOAT, 0, parr)
    gl.glColorPointer(4, gl.GL_FLOAT, 0, carr)
    gl.glDrawArrays(gl.GL_LINES, 0, self.nline*2)


class Renderer(object):
  def __init__(self, engine):
    self.engine = engine
    self.scale = 2
    self._spritesheets = {}
    self._layers = defaultdict(Layer)
    self.clearcolour = None
    self.fb = Framebuffer()

  def resize(self, w, h):
    self.dw, self.dh = w, h
    self.w = w/self.scale
    self.h = h/self.scale
    gl.glViewport(0,0,w,h)
    gl.glMatrixMode(gl.GL_PROJECTION)
    gl.glLoadIdentity()
    gl.glOrtho(0,w,h,0,0,1)
    gl.glMatrixMode(gl.GL_MODELVIEW)
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
    gl.glEnable(gl.GL_TEXTURE_2D)
    if self.clearcolour:
      gl.glClearColor(*self.clearcolour)
    self.tex = Texture(w, h)
    self.fb.set_texture(self.tex)

  def texture(self, fn):
    img = pyglet.image.load(fn)
    tex = img.get_texture()
    gl.glBindTexture(tex.target, tex.id)
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER,gl.GL_NEAREST)
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER,gl.GL_NEAREST)
    return tex, img

  def startFrame(self):
    gl.glLoadIdentity()
    for lid in self._layers:
      self._layers[lid].reset()

  def drawrect(self, x0,y0,x1,y1,col,layer=10):
    l = self.layer("", layer)
    l.texquad(x0, y0, x1, y0, x1, y1, x0, y1, col=col)

  def drawline(self, x0,y0,x1,y1,col,layer=10):
    l = self.layer("", layer)
    l.line(x0, y0, x1, y1, col=col)

  def drawcircle(self, x,y,r,col,layer=10):
    l = self.layer("", layer)
    n = 32
    for i in range(n):
      a0 = i/float(n)*math.pi*2
      a1 = (i+1)/float(n)*math.pi*2
      x0,y0 = x+math.cos(a0)*r, y+math.sin(a0)*r
      x1,y1 = x+math.cos(a1)*r, y+math.sin(a1)*r
      l.line(x0, y0, x1, y1, col=col)


  def layer(self, fn, layer):
    return self._layers[(layer, fn)]

  def sprites(self, fn, w=None, h=None, centre=(0,0)):
    if fn not in self._spritesheets:
      self._spritesheets[fn] = SpriteSheet(self, fn, w, h, centre=centre)
    return self._spritesheets[fn]

  def font(self, fn, w, h):
    if fn not in self._spritesheets:
      self._spritesheets[fn] = Font(self, fn, w, h)
    return self._spritesheets[fn]

  def vfont(self, fn):
    if fn not in self._spritesheets:
      self._spritesheets[fn] = VariableFont(self, fn)
    return self._spritesheets[fn]

  def endFrame(self):
    self.fb.bind()
    gl.glClear(gl.GL_COLOR_BUFFER_BIT)
    for layer, fn in sorted(self._layers):
      if fn in self._spritesheets:
        s = self._spritesheets[fn]
        gl.glEnable(gl.GL_TEXTURE_2D)
        gl.glBindTexture(gl.GL_TEXTURE_2D, s.tex.id)
      else:
        gl.glDisable(gl.GL_TEXTURE_2D)
      self._layers[(layer, fn)].draw()
    #self.fb.check()
    self.fb.unbind()

    self.tex.bind()
    gl.glEnable(gl.GL_TEXTURE_2D)
    s = self.scale*self.scale
    w, h = self.w*s, self.h*s
    gl.glBegin(gl.GL_QUADS)
    gl.glTexCoord2f(0, 1); gl.glVertex2f(0, 0)
    gl.glTexCoord2f(1, 1); gl.glVertex2f(w, 0)
    gl.glTexCoord2f(1, 0); gl.glVertex2f(w, h)
    gl.glTexCoord2f(0, 0); gl.glVertex2f(0, h)
    gl.glEnd()
    self.tex.unbind()
