# vim:set ts=2 sw=2 sts=2 et:

import math


def npo2(v):
  v-=1
  v |= v >> 1
  v |= v >> 2
  v |= v >> 4
  v |= v >> 8
  v |= v >> 16
  return v+1


class SpriteSheet(object):
  def __init__(self, render, fn, tilew=None, tileh=None, centre=(0,0)):
    self.render = render
    self.fn = fn
    self.tex, img = render.texture(fn)
    if not tilew: tilew = img.width
    if not tileh: tileh = img.height
    self.tilesize = (tilew, tileh)
    self.texsize = (npo2(img.width), npo2(img.height))
    self.framecount = img.width/tilew, img.height/tileh
    self.framesize = tilew/float(self.texsize[0]), tileh/float(self.texsize[1])
    self.centre = tuple(centre)

  def _drawQuad(self, layer, x, y, w, h, tx0, ty0, tx1, ty1, angle, scale, col):
    cx, cy = self.centre
    sx0 = ( -cx)*scale
    sy0 = ( -cy)*scale
    sx1 = (w-cx)*scale
    sy1 = ( -cy)*scale
    sx2 = (w-cx)*scale
    sy2 = (h-cy)*scale
    sx3 = ( -cx)*scale
    sy3 = (h-cy)*scale
    if angle:
      ca = math.cos(angle)
      sa = math.sin(angle)
      def tfm(x, y): return (x*ca-y*sa), (x*sa+y*ca)
      sx0, sy0 = tfm(sx0, sy0)
      sx1, sy1 = tfm(sx1, sy1)
      sx2, sy2 = tfm(sx2, sy2)
      sx3, sy3 = tfm(sx3, sy3)

    col = col if col else (1,1,1,1)
    layer.texquad(x+sx0,y+sy0, x+sx1,y+sy1, x+sx2,y+sy2, x+sx3,y+sy3,
                  tx0, ty0, tx1, ty1, col)
  
  def drawFrame(self, layer, x, y, frame, col, angle=0, flip=False, scale=1):
    w,h = self.tilesize
    fw, fh = self.framesize
    fx = frame%self.framecount[0]
    fy = self.framecount[1]-1-(frame/self.framecount[0])
    tx0 = (fx  )*fw
    tx1 = (fx+1)*fw
    ty0 = (fy+1)*fh
    ty1 = (fy  )*fh
    if flip:
      tx0, tx1 = tx1, tx0

    self._drawQuad(layer, x, y, w, h, tx0, ty0, tx1, ty1, angle, scale, col)
  
  def drawRaw(self, x, y, px0, py0, px1, py1, layer=0, col=None, angle=0, flip=False, scale=1):
    layer = self.render.layer(self.fn, layer)
    w,h = (px1-px0), (py1-py0)
    tx0 = px0/float(self.texsize[0])
    tx1 = px1/float(self.texsize[0])
    ty0 = py1/float(self.texsize[1])
    ty1 = py0/float(self.texsize[1])
    if flip:
      tx0, tx1 = tx1, tx0

    self._drawQuad(layer, x, y, w, h, tx0, ty0, tx1, ty1, angle, scale, col)

  def draw(self, x, y, frame=0, layer=0, col=None, angle=0, flip=False, scale=1):
    l = self.render.layer(self.fn, layer)
    self.drawFrame(l, x, y, frame, col, angle, flip, scale)

