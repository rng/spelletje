# vim:set ts=2 sw=2 sts=2 et:

from ctypes import byref
import pyglet.gl as gl


class Texture(object):
  def __init__(self, w, h):
    self.id = gl.GLuint()
    gl.glGenTextures(1, byref(self.id))
    buf = (gl.GLubyte * (w*h*3))()
    self.bind()
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER,gl.GL_NEAREST)
    gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER,gl.GL_NEAREST)
    gl.glTexImage2D(
      gl.GL_TEXTURE_2D, 0, gl.GL_RGB, 
      w, h, 
      0, gl.GL_RGB, gl.GL_UNSIGNED_BYTE, 
      buf
    )
    self.unbind()

  def bind(self):
    gl.glEnable(gl.GL_TEXTURE_2D)
    gl.glBindTexture(gl.GL_TEXTURE_2D, self.id)
    
  def unbind(self):
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
    gl.glDisable(gl.GL_TEXTURE_2D)


class Framebuffer(object):

  def __init__(self):
    if not gl.gl_info.have_extension('GL_EXT_framebuffer_object'):
      raise Exception("No gl framebuffer support")
    id = gl.GLuint()
    gl.glGenFramebuffersEXT(1, byref(id))
    self.id = id.value
  
  def bind(self):
    gl.glBindFramebufferEXT(gl.GL_FRAMEBUFFER_EXT, gl.GLuint(self.id))

  def unbind(self):
    gl.glBindFramebufferEXT(gl.GL_FRAMEBUFFER_EXT, gl.GLuint(0))

  def check(self):
    status = gl.glCheckFramebufferStatusEXT(gl.GL_FRAMEBUFFER_EXT)
    if status != gl.GL_FRAMEBUFFER_COMPLETE_EXT:
      raise Exception('Framebuffer error: %d' % status)

  def set_texture(self, texture, n=0):
    self.bind()
    gl.glFramebufferTexture2DEXT(
      gl.GL_FRAMEBUFFER_EXT,
      gl.GL_COLOR_ATTACHMENT0_EXT + n,
      gl.GL_TEXTURE_2D,
      texture.id,
      0,
    )
    self.unbind()

