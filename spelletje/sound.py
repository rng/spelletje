# vim:set ts=2 sw=2 sts=2 et:

import pyglet
import glob
import os


class SoundManager(object):
  def __init__(self, engine):
    self.engine = engine
    self.sounds = {}
    for fn in glob.glob(os.path.join("data","sound","*.wav")):
      self.sounds[fn] = pyglet.resource.media(fn.replace("\\","/"), streaming=False)

  def play(self, fn):
    fn = os.path.join("data","sound","%s.wav" % fn)
    if fn not in self.sounds:
      raise Exception("Can't find audio: %s" % fn)
    self.sounds[fn].play()
