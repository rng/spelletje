# vim:set ts=2 sw=2 sts=2 et:

import math


class Entity(object):
  def __init__(self, engine, props):
    self.engine = engine
    self.__dict__.update(props)
    self.collides = False
    self.w = self.h = 0
    self.centre = (0,0)
    self.friction = (1., 1.)
    self.bounciness = (0., 0.)
    self.gravity = 0
    self.alive = True
    self.drawbb = False
    self.nx = props.get("x",0)
    self.ny = props.get("y",0)
    self.vx = props.get("vx", 0)
    self.vy = props.get("vy", 0)
    self.flip = props.get("flip", False)

  def setPos(self, x, y):
    self.nx, self.ny = x, y

  def bounds(self):
    cx, cy = self.centre
    return self.x-cx, self.y-cy, self.x-cx+self.w, self.y-cy+self.h

  def touches(self, other):
    sx1, sy1, sx2, sy2 = self.bounds()
    ox1, oy1, ox2, oy2 = other.bounds()
    return (sx1 <= ox2) and (sx2 >= ox1) and (sy1 <= oy2) and (sy2 >= oy1)

  def collide(self, other):
    pass

  def update(self, dt):
    self.vy += self.gravity*30*dt
    self.vx *= self.friction[0]
    self.vy *= self.friction[1]
    if self.collides:
      r = self.game.map.collide(self.x, self.y, 
                                self.vx*30*dt, self.vy*30*dt, 
                                self.w, self.h)
      if r.collx:
        self.vx *= -self.bounciness[0]
        self.flip = self.vx < 0
      if r.colly:
        self.vy *= -self.bounciness[1]
      self.setPos(r.px, r.py)
      return r
    else:
      self.setPos(self.x + self.vx, self.y + self.vy)

  def viewPos(self):
    ox, oy = self.engine.camera.origin()
    return int(self.x - ox), int(self.y - oy)

  def distanceTo(self, other):
    return math.hypot(self.x-other.x, self.y-other.y)

  def draw(self):
    if self.drawbb:
      x, y = self.viewPos()
      x -= self.centre[0]
      y -= self.centre[1]
      self.engine.render.drawrect(x, y, x+self.w, y+self.h, [0,1,0,0.5])

  def remove(self):
    self.alive = False
    self.engine.entities.remove(self)


class EntityManager(object):
  def __init__(self, engine):
    self.engine = engine
    self.entities = []
    self.removes = []
  
  def spawn(self, et, **params):
    e = et(self.engine, params)
    self.entities.append(e)
    return e

  def clear(self):
    for e in self.entities:
      self.remove(e)

  def remove(self, e):
    if e not in self.removes:
      self.removes.append(e)

  def ofType(self, t):
    return [e for e in self.entities if isinstance(e, t)]

  def nearest(self, src, t=Entity):
    mindist = 100000000
    nearest = None
    for e in self.entities:
      if isinstance(e, t) and e is not src:
        d = math.hypot(e.x-src.x, e.y-src.y)
        if d < mindist:
          nearest = e
          mindist = d
    return nearest, mindist

  def update(self, dt):
    # update
    for e in self.entities:
      ox, oy = e.x, e.y
      e.update(dt)
      assert ox == e.x
      assert oy == e.y
    # check collisions
    self.collide()
    # set new positions
    for e in self.entities:
      e.x, e.y = e.nx, e.ny
    # do removes
    for e in self.removes:
      self.entities.remove(e)
    self.removes = []

  def draw(self):
    for e in self.entities:
      e.draw()

  def collide(self):
    collisions = set()
    # find collisions
    for a in self.entities:
      if a.collides:
        for b in self.entities:
          if b.collides and b is not a:
            if a.touches(b) and a.alive and b.alive:
              collisions.add((a,b))
              collisions.add((b,a))
    # process them
    for a, b in collisions:
      a.collide(b)
