# vim:set ts=2 sw=2 sts=2 et:

import math


class Slide(object):
  LINEAR = "linear"
  SIN = "sin"
  SIN2 = "sin2"
  OVERSIN = "oversin"

  def __init__(self, start, end, tmax, loop=False, fn=SIN):
    self.start, self.end, self.tmax = start, end, tmax
    self.pos = start
    self.t = 0
    self.loop = loop
    self.done = False
    self.fn = fn

  def update(self, dt):
    self.t += dt
    if self.t >= self.tmax:
      if self.loop:
        self.t = self.t % self.tmax
      else:
        self.t = self.tmax
        self.done = True
    fns = {"sin": lambda v: math.sin(v*math.pi/2.),
           "sin2" : lambda v: math.sin(v*math.pi),
           "oversin" : lambda v: math.sin(v*math.pi*.6)/(math.sin(.6*math.pi)),
           "linear" : lambda v: v}
    ofs = (self.end-self.start)*fns[self.fn](self.t/self.tmax)
    self.pos = self.start + ofs


class Transition(object):
  def __init__(self, engine, newstate, tmax, args, first=False):
    self.engine = engine
    if first:
      self.t, self.tmax = tmax, tmax*2.
    else:
      self.t, self.tmax = 0, tmax
    self.args = args
    self.newstate = newstate
    self.done = self.donehalf = False

  def update(self, dt):
    if self.t >= self.tmax/2.:
      if not self.donehalf:
        self.donehalf = True
        self.engine.enterState(self.newstate, self.args)
    if self.t >= self.tmax:
      self.t = self.tmax
      self.done = True
    self.t += dt
    return self.done

  def draw(self):
    fn = lambda v: 1-math.sin(v*math.pi)
    v = fn(self.t/self.tmax)
    self.engine.render.drawrect(0,0,240,160,[0,0,0,1-v])


class Animation(object):
  def __init__(self, delay, init, **anims):
    self.anims = anims
    self.frame = 0
    self.ofs = 0
    self.delay = delay
    self.t = 0
    self.curanim = init
    self.loop = anims.get("loop", True)
    self.done = False

  def play(self, anim):
    if self.curanim != anim:
      self.curanim = anim

  def update(self, dt):
    self.t += dt
    if self.t > self.delay:
      self.t = 0
      if ((self.ofs+1) == self.anims[self.curanim][1]) and not self.loop:
        self.done = True
      else:
        self.ofs = (self.ofs+1) % self.anims[self.curanim][1]
      self.frame = self.ofs + self.anims[self.curanim][0]
