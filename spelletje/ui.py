# vim:set ts=2 sw=2 sts=2 et:

import math


class Button(object):
  def __init__(self, gs, x, y, t, toggle=False, cb=None):
    self.x, self.y, self.t = x, y, t
    self.gs = gs
    self.cb = cb
    self.press = self.hover = False
    self.toggle = toggle
    self.toggled = False
    self.visible = True
    self.layer = 1

  def draw(self):
    if self.visible:
      self.gs.button.draw(self.x, self.y, 1 if (self.press or self.toggled) else 0)
      self.gs.button.draw(self.x, self.y, self.t + (6 if self.hover else 2))

  def inthis(self, x, y):
    if not self.visible:
      return False
    x1, x2 = self.x+1, self.x+15
    y1, y2 = self.y+1, self.y+15
    return (x1 <= x <= x2) and (y1 <= y <= y2)

  def on_press(self, x, y):
    if self.inthis(x, y):
      self.press = True

  def on_move(self, x, y):
    self.hover = self.inthis(x, y)

  def on_release(self, x, y):
    self.press = False
    if self.inthis(x, y):
      if self.toggle:
        self.toggled = not self.toggled
      if self.cb:
        self.cb(self)


class TextButton(Button):
  def __init__(self, gs, x, y, text, toggle=False, centre=False, cb=None, col=None):
    Button.__init__(self, gs, x, y, 0, cb=cb)
    self.col = col if col else [1,1,1,1]
    self.text = text
    self.centre = centre
  
  def inthis(self, x, y):
    if not self.visible:
      return False
    w = len(self.text)*4
    h = 6
    o = w/2 if self.centre else 0
    x1, x2 = self.x-o-1, self.x-o+w+1
    y1, y2 = self.y-1, self.y+h+1
    return (x1 <= x <= x2) and (y1 <= y <= y2)
  
  def draw(self):
    if self.visible:
      col = list(self.col)
      if not self.hover:
        col[3] *= 0.8
      self.gs.font.draw(self.x,self.y, self.text, layer=self.layer, col=col, centre=self.centre)


class Dragable(object):
  def __init__(self, gs, x, y, t):
    self.x, self.y, self.t = x, y, t
    self.gs = gs
    self.press = self.hover = False
    self.xo = self.yo = 0
    self.sx = self.sy = 0
    self.nearest = None

  def draw(self):
    if self.press:
      self.gs.highlight.draw(self.x-6,self.y-6, 0, 3)
      self.gs.tiles.draw(self.x-1, self.y-1, self.t, 4)
    else:
      if self.y != 72:
        self.gs.tiles.draw(self.x, self.y, self.t)
    if self.hover and not self.press:
      self.gs.highlight.draw(self.x-8,self.y-8, 1)
  
  def inthis(self, x, y):
    x1, x2 = self.x, self.x+15
    y1, y2 = self.y, self.y+15
    return (x1 <= x <= x2) and (y1 <= y <= y2)

  def on_press(self, x, y):
    if self.inthis(x, y):
      self.press = True
      self.xo = x-self.x
      self.yo = y-self.y
      self.xs = self.x
      self.ys = self.y
      self.gs.removeFromSlot(self)

  def on_move(self, x, y):
    self.hover = self.inthis(x, y)

  def on_release(self, x, y):
    if self.press and self.inthis(x, y):
      self.on_drag(x,y)
      if self.nearest and self.nearest.highlight:
        self.x = self.nearest.x
        self.y = self.nearest.y
        self.gs.moveToSlot(self, self.nearest)
      else:
        self.x = self.xs
        self.y = self.ys
        self.on_drag(self.x+self.xo,self.y+self.yo)
        self.gs.moveToSlot(self, self.nearest)
      for ds in self.gs.slots:
        ds.highlight = False
    self.press = False

  def on_drag(self, x, y):
    if self.press:
      self.x = x-self.xo
      self.y = y-self.yo
      nearest = None
      mdist = 1000000
      for ds in self.gs.slots:
        ds.highlight = False
        if not ds.contains:
          dist = math.hypot(self.x - ds.x, self.y - ds.y)
          if dist < mdist:
            mdist = dist
            nearest = ds
      if nearest and mdist < 20:
        nearest.highlight = True
      self.nearest = nearest


class DragSlot(object):
  def __init__(self, gs, x, y, c=None):
    self.x, self.y  = x, y
    self.gs = gs
    self.highlight = False
    self.contains = c
  
  def draw(self):
    if self.highlight:
      self.gs.highlight.draw(self.x-8,self.y-8, 1)


class UI(object):
  def __init__(self, gs):
    self.gs = gs
    self.widgets = []

  def add(self, w):
    self.widgets.append(w)
    return w
  
  def draw(self):
    [w.draw() for w in self.widgets]

  def update(self, dt):
    pass

  def key_press(self, sym, mods):
    pass
  
  def mouse_motion(self, x, y, dx, dy):
    [w.on_move(x, y) for w in self.widgets]

  def mouse_press(self, x, y, button, mods):
    [w.on_press(x, y) for w in self.widgets]

  def mouse_release(self, x, y, button, mods):
    [w.on_release(x, y) for w in self.widgets]
  
  def mouse_drag(self, x, y, dx, dy, buttons, mods):
    pass

