#!/usr/bin/env python
# vim:set ts=2 sw=2 sts=2 et:

import pyglet


class Window(pyglet.window.Window):
  def __init__(self, engine, w, h, framerate):
    pyglet.window.Window.__init__(self,width=w, height=h)
    self.engine = engine
    self.engine.resize(w, h)
    self.engine.start(self)
    pyglet.clock.schedule_interval(self.update, 1/float(framerate))

  def on_resize(self, w, h):
    self.engine.resize(w, h)

  def on_key_press(self, sym, mods):
    self.engine.key_press(sym, mods)

  def on_key_release(self, sym, mods):
    self.engine.key_release(sym, mods)

  def on_mouse_motion(self, x, y, dx, dy):
    self.engine.mouse_motion(x, y, dx, dy)

  def on_mouse_press(self, x, y, button, mods):
    self.engine.mouse_press(x, y, button, mods)

  def on_mouse_release(self, x, y, button, mods):
    self.engine.mouse_release(x, y, button, mods)

  def on_mouse_drag(self, x, y, dx, dy, buttons, mods):
    self.engine.mouse_drag(x, y, dx, dy, buttons, mods)

  def on_mouse_scroll(self, x, y, sx, sy):
    self.engine.mouse_scroll(x, y, sx, sy)

  def on_draw(self):
    self.engine.draw()

  def update(self, dt):
    self.engine.update(dt)
    if self.engine.done():
      self.close()


def run(engine, w=480, h=320, framerate=30):
  Window(engine, w, h, framerate)
  pyglet.app.run()
