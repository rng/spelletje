# vim:set ts=2 sw=2 sts=2 et:

from .sprite import SpriteSheet, npo2
import json

class Font(SpriteSheet):
  def draw(self, x, y, s, centre=False, layer=0, col=None):
    batch = self.render.layer(self.fn, layer)
    ox = len(s)*self.tilesize[0]/2 if centre else 0
    for i,c in enumerate(s):
      cx = x - ox + i*self.tilesize[0]
      super(Font, self).drawFrame(batch, cx, y, ord(c)-32, col)


class VariableFont(object):
  def __init__(self, render, fn):
    self.render = render
    self.fn = fn
    self.tex, img = render.texture(fn)
    self.chars = json.load(open(fn.replace(".png",".json")))
    self.texsize = (npo2(img.width), npo2(img.height))

  def _charw(self, c):
    if c == ' ':
      return self.chars[ord('j')]["w"]
    return self.chars[ord(c)]["w"]
  
  def _charh(self, c):
    return self.chars[ord(c)]["h"]

  def drawChar(self, batch, x, y, char, col, scale):
    c = ord(char)
    tw, th = self.texsize
    w = self.chars[c]["w"]
    h = self.chars[c]["h"]
    if w == 0:
      return
    x0 = x
    y0 = y
    x1 = x+w*scale
    y1 = y+h*scale
    cx = self.chars[c]["x"]
    cy = self.chars[c]["y"]
    tx0 = cx/float(tw)
    ty0 = 1-cy/float(th)
    tx1 = (cx+w)/float(tw)
    ty1 = 1-(cy+h)/float(th)
    col = col if col else (1,1,1,1)
    batch.texquad(x0,y0, x1,y0, x1,y1, x0,y1,
                  tx0,ty0,tx1,ty1, col)

  def size(self, s, scale=1, charpad=0):
    w = h = 0
    for c in s:
      w += (self._charw(c)+charpad)*scale
      h = max(h, self._charh(c)*scale)
    w -= scale
    return w, h

  def draw(self, x, y, s, centre=False, layer=0, col=None, scale=1, charpad=0):
    if centre:
      w, _ = self.size(s, scale, charpad=charpad)
      x -= w/2
    batch = self.render.layer(self.fn, layer)
    for i, c in enumerate(s):
      self.drawChar(batch, x, y, c, col, scale)
      x += (self._charw(c)+charpad)*scale

