# vim:set ts=2 sw=2 sts=2 et:

from .render import Renderer
from .camera import Camera
from .entity import EntityManager
from .animation import Transition
from .input import Input
from .state import State
from .save import Saver
from .sound import SoundManager
import time
import pyglet

class Engine(object):
  def __init__(self, states, startstate, title=None, icon=None):
    self._done = False
    self.render = Renderer(self)
    self.camera = Camera(self)
    self.input = Input(self)
    self.sound = SoundManager(self)
    self.save = Saver()
    self.states = dict([(k,v(self)) for (k,v) in states.items()])
    self.startstate = startstate
    self.title = title
    self.icon = icon
    self.framecount = 0
    self.t0 = time.time()
    self.fps = 0
    self.entities = EntityManager(self)

  def resize(self, w, h):
    self.render.resize(w, h)

  def start(self, window):
    if self.title:
      window.set_caption(self.title)
    if self.icon:
      window.set_icon(pyglet.image.load(self.icon))
    self.window = window
    self.setScale(self.save.get("res_scale", 2))
    self.states["null"] = State(self)
    self.state = "null"
    self.switchState(self.startstate, 0.6, True)

  def setScale(self, scale):
    assert scale in range(1,5)
    self.render.scale = scale
    self.window.set_size(self.render.w*scale, self.render.h*scale)

  def showCursor(self, v=True):
    self.window.set_mouse_visible(v)

  def shutdown(self):
    self.save.write()

  def switchState(self, newstate, t=0.4, first=False, args=[]):
    self.transition = Transition(self, newstate, t, first=first, args=args)

  def enterState(self, newstate, args):
    self.states[self.state].leaveState()
    self.state = newstate
    self.states[self.state].enterState(args)

  def draw(self):
    self.render.startFrame()
    self.states[self.state].draw()
    self.entities.draw()
    if self.transition:
      self.transition.draw()
    self.render.endFrame()
    self.states[self.state].postDraw()
    self.framecount += 1
    dt = time.time() - self.t0
    if dt > 0.5:
      fps = self.framecount/float(dt)
      self.fps = (self.fps+fps)/2.
      self.framecount = 0
      self.t0 = time.time()

  def update(self, dt):
    if self.transition:
      if self.transition.update(dt):
        self.transition = None
    self.states[self.state].update(dt)
    self.entities.update(dt)

  def quit(self):
    self._done = True

  def done(self):
    self.shutdown()
    return self._done
  
  def key_press(self, sym, mods):
    self.input.key_press(sym, mods)
    if self.transition: return
    self.states[self.state].key_press(sym, mods)

  def key_release(self, sym, mods):
    self.input.key_release(sym, mods)
    if self.transition: return
    self.states[self.state].key_release(sym, mods)

  def mouse_motion(self, x, y, dx, dy):
    x = x/self.render.scale
    y = self.render.h-y/self.render.scale
    self.input.mouse_motion(x, y, dx, dy)
    self.states[self.state].mouse_motion(x, y, dx, dy)

  def mouse_press(self, x, y, button, mods):
    x = x/self.render.scale
    y = self.render.h-y/self.render.scale
    self.input.mouse_press( x, y, button, mods)
    self.states[self.state].mouse_press(x, y, button, mods)

  def mouse_release(self, x, y, button, mods):
    x = x/self.render.scale
    y = self.render.h-y/self.render.scale
    self.input.mouse_release(x, y, button, mods)
    self.states[self.state].mouse_release(x, y, button, mods)

  def mouse_drag(self, x, y, dx, dy, buttons, mods):
    x = x/self.render.scale
    y = self.render.h-y/self.render.scale
    self.input.mouse_drag(x, y, dx, dy, buttons, mods)
    self.states[self.state].mouse_drag(x, y, dx, dy, buttons, mods)

  def mouse_scroll(self, x, y, sx, sy):
    x = x/self.render.scale
    y = self.render.h-y/self.render.scale
    self.states[self.state].mouse_scroll(x, y, sx, sy)

