#!/usr/bin/env python

from setuptools import setup

setup(name='Spelletje',
  version='0.1',
  description='Small Python Game Library',
  author='Ross Glashan',
  author_email='glashan@gmail.com',
  url='',
  packages=['spelletje', 'spelletje.backend'],
)
